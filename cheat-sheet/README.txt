The file ending in inkscape.src.svg contains the Inkscape source file,
the other svg file has the text converted to path and is saved without
inkscape extensions.

The latter file is more reliable to use on the web, but changes are best
made first to the inkscape source.

