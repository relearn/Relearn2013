#!/usr/bin/env python

'''
Basic usage:

    >>> text = """Super
    ... Cool"""
    >>> print(make_oblique(text))
    S   C
     u   o
      p   o
       e   l
        r    
    <BLANKLINE>
'''

import sys
from textwrap import dedent
import fileinput
import numpy as np


def make_oblique(text, line_spacing=1, slant=1, letter_spacing=1, reverse=False):
    lines = text.splitlines()  # Splits the text at newlines
    max_len = max([len(x) for x in lines])
    lines = [x.ljust(max_len) for x in lines]

    if reverse:
        lines.reverse()

    # Computes the size of or canvas
    n_row = (len(max(lines)) + (len(lines) * line_spacing)) * slant
    n_col = len(max(lines)) + (len(max(lines)) * letter_spacing)

    # Creates the canvas
    canvas = np.zeros((n_row, n_col), dtype='S1')
    canvas[...] = ' '  # ye mighty FORTRAN, we beseech thee

    for i, line in enumerate(lines):
        letters_x = np.multiply(np.arange(len(line)), letter_spacing)
        letters_y = np.multiply(np.arange(len(line)), slant)
        canvas[letters_y + (i * line_spacing), letters_x] = list(line)

    canvas[:, -1] = '\n'
    if reverse:
        canvas = canvas[::-1]
    return canvas.tostring().rstrip()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Makes a text oblique')
    parser.add_argument('-t', '--text', type=unicode, default=None, help='The text to process')
    parser.add_argument('-v', '--line-spacing', type=int, default=1, help='The line spacing')
    parser.add_argument('-s', '--slant', type=int, default=1, help='The slant')
    parser.add_argument('-c', '--letter-spacing', type=int, default=1, help='The slant')
    parser.add_argument('-r', '--reverse', default=False, action="store_true", help='Invert direction')

    args = parser.parse_args()

    if args.text is None:
        print(make_oblique(sys.stdin.read(), line_spacing=args.line_spacing, slant=args.slant, letter_spacing=args.letter_spacing, reverse=args.reverse))
    else:
        print(make_oblique(args.text, line_spacing=args.line_spacing, slant=args.slant, letter_spacing=args.letter_spacing, reverse=args.reverse))
