#!/bin/python
# -*- coding: utf-8 -*-
"""
premier test de trancheur texte
"""

import sys

# get the name of the file containing the text
txt_file = sys.argv[1]

# open this file
f = open(txt_file)

# read the content of the file and put it in a variable for later use
txt = f.read()

# get the length of the text
lt = len(txt)

slicedText = "" 

# get the length of a slice from the command line, if missing, pretend it's 32
try:
    sl = sys.argv[2]
except Exception:
    sl = 32
    
    
try:
    pc = sys.argv[3]
except Exception:
    pc = " "

for i in range(0, lt, sl):
    slic = txt[i:i+sl]
    if len(slic) < sl:
        dif = sl - len(slic)
        padding = pc * dif
        slic = slic + padding
    print "\"%s\","% (slic,)
    slicedText += "\"%s\",\n"% (slic,)

w = len(slicedText.split("\","))-1
    
    
header = """
/* XPM */
static char *moon[] = {
    /* columns rows colors chars-per-pixel */
    "%s %s 92 1 ",
"  c #A8D092",
"! c #E92F30",
"# c #83B492",
"$ c #A17095",
"& c #2BFE30",
"' c #328F41",
"( c #EB3A1C",
") c #2541BE",
"* c #85F2A9",
"+ c #6C7480",
", c #BE2FA8",
"- c #3178CB",
". c #58DA43",
"/ c #294785",
"0 c #690B23",
"1 c #AFD5EC",
"2 c #957A03",
"3 c #E745C0",
"4 c #A53E81",
"5 c #8F720C",
"6 c #9E4FBA",
"7 c #869C21",
"8 c #E8ACD7",
"9 c #6B47D5",
": c #4D1C7A",
"; c #7A15C4",
"< c #C0B79A",
"= c #190F2E",
"> c #E526CF",
"? c #6250C1",
"@ c #92A8B0",
"A c #1A9D76",
"B c #B9D5E3",
"C c #734AD9",
"D c #49D5AF",
"E c #7DB403",
"F c #623E0B",
"G c #E96DCB",
"H c #96A358",
"I c #EF532D",
"J c #08719C",
"K c #593A68",
"L c #581BA9",
"M c #435B61",
"N c #19C086",
"O c #420DC7",
"P c #78F1D4",
"Q c #6BA42D",
"R c #981457",
"S c #FB9D2E",
"T c #492013",
"U c #FCB705",
"V c #FC2BE0",
"W c #3F19D0",
"X c #865CE1",
"Y c #38F26D",
"Z c #6FD0E5",
"[ c #C729DA",
"\ c #E46B72",
"] c #5A7F02",
"^ c #B45E1A",
"_ c #28931A",
"` c #D6A802",
"a c #9E43CF",
"b c #609D57",
"c c #6D1F90",
"d c #368DFE",
"e c #A56317",
"f c #E46DBA",
"g c #5CD7F9",
"h c #F0278C",
"i c #59C2AD",
"j c #67ECFB",
"k c #1D2946",
"l c #8B1D7A",
"m c #A38BD1",
"n c #AB0EF4",
"o c #460A81",
"p c #243DE8",
"q c #51E7C9",
"r c #EF458C",
"s c #4376B1",
"t c #438FD2",
"u c #BAC607",
"v c #FAD8EC",
"w c #04A9E3",
"x c #4182BC",
"y c #71539E",
"z c #9846D1",
"{ c #D1946B",
"| c #DC49F1",
"} c #9E8A7B",
"~ c #4152B8",
    """ % (sl, w)
    
footer = "};"

export = header + slicedText + footer
file = open("output.xpm","w")
file.write(export)
file.close()
        